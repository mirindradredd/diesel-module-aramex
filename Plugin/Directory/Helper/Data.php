<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Plugin\Directory\Helper;

/**
 * Class Data
 * @package Beside\Aramex\Plugin\Directory\Helper
 */
class Data
{
    /**
     * @var \Beside\Aramex\Helper\Data
     */
    private $aramexHelper;

    /**
     * Data constructor.
     * @param \Beside\Aramex\Helper\Data $aramexHelper
     */
    public function __construct(
        \Beside\Aramex\Helper\Data $aramexHelper
    ) {
        $this->aramexHelper = $aramexHelper;
    }

    /**
     * @param \Magento\Directory\Helper\Data $subject
     * @param $result
     */
    public function afterGetRegionData(
        \Magento\Directory\Helper\Data $subject,
        $result
    ) {
        $cities = $this->aramexHelper->getCities();
        foreach($result as $country=>&$regions)
        {
            foreach($regions as $regionId=>&$region)
            {
                if(isset($cities[$regionId])){
                    $region['cities'] = $cities[$regionId];
                }
            }
        }

        return $result;
    }
}
