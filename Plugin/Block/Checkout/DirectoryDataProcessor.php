<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Plugin\Block\Checkout;

/**
 * Class DirectoryDataProcessor
 * @package Beside\Aramex\Plugin\Block\Checkout
 */
class DirectoryDataProcessor
{
    /**
     * @var \Beside\Aramex\Helper\Data
     */
    private $aramexHelper;

    /**
     * DirectoryDataProcessor constructor.
     * @param \Beside\Aramex\Helper\Data $aramexHelper
     */
    public function __construct(
        \Beside\Aramex\Helper\Data $aramexHelper
    ) {
        $this->aramexHelper = $aramexHelper;
    }

    /**
     * @param \Magento\Checkout\Block\Checkout\DirectoryDataProcessor $subject
     * @param $result
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\DirectoryDataProcessor $subject,
        $result
    ) {
        if (isset($result['components']['checkoutProvider']['dictionaries']['region_id'])) {
            $cities = $this->aramexHelper->getCities();
            foreach ($result['components']['checkoutProvider']['dictionaries']['region_id'] as &$region) {
                if (isset($cities[$region['value']])) {
                    $region['cities'] = $cities[$region['value']];
                }
            }
        }

        return $result;
    }
}
