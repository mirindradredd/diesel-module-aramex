<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface CityInterface extends ExtensibleDataInterface
{
    const CITY_ID = 'city_id';
    const REGION_ID = 'region_id';
    const NAME = 'name';

    /**
     * @api
     * @return int|null
     */
    public function getCityId();

    /**
     * @api
     * @param int $cityId
     * @return $this
     */
    public function setCityId($cityId);

    /**
     * @api
     * @return int|null
     */
    public function getRegionId();

    /**
     * @api
     * @param int $regionId
     * @return $this
     */
    public function setRegionId($regionId);

    /**
     * @api
     * @return string|null
     */
    public function getName();

    /**
     * @api
     * @param string $name
     * @return $this
     */
    public function setName($name);
}
