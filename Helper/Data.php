<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Aramex\Helper;

use Beside\Aramex\Model\ResourceModel\City\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Cache\Type\Config;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_ARAMEX_TRACKING_LINK = 'carriers/aramex/base_tracking_link';
    /**
     * @var array
     */
    private $citiesArray = [];
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var Config
     */
    private $configCacheType;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Data constructor.
     * @param CollectionFactory $collectionFactory
     * @param Config $configCacheType
     * @param SerializerInterface $serializer
     * @param Context $context
     */
    public function __construct(
        \Beside\Aramex\Model\ResourceModel\City\CollectionFactory $collectionFactory,
        \Magento\Framework\App\Cache\Type\Config $configCacheType,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        \Magento\Framework\App\Helper\Context $context,
        StoreManagerInterface $storeManager
    ){
        $this->collectionFactory = $collectionFactory;
        $this->configCacheType = $configCacheType;
        $this->serializer = $serializer;
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getCities()
    {
        if (!$this->citiesArray) {
            $cacheKey = 'DIRECTORY_CITIES_JSON';
            
            //$json = $this->configCacheType->load($cacheKey);
            $json = "";

            if (empty($json)) {
                $cityCollection = $this->collectionFactory->create();
                foreach ($cityCollection as $city) {
                    $this->citiesArray[] = [
                        'value' => $city->getId(),
                        'name' => $city->getName(),
                        'region_id' => $city->getRegionId(),
                        'name2' => __($city->getName())
                    ];
                }
                $json = $this->serializer->serialize($this->citiesArray);
                if ($json === false) {
                    $json = 'false';
                }

                $this->configCacheType->save($json, $cacheKey);
                if ($json !== false) {
                    $this->citiesArray = $this->serializer->unserialize($json);
                }

            } else {
                $this->citiesArray = $this->serializer->unserialize($json);
            }
        }

        return $this->citiesArray;
    }

    /**
     * @return string
     */
    public function getTrackingUrl()
    {
        return (string) $this->scopeConfig->getValue(
            self::XML_PATH_ARAMEX_TRACKING_LINK,
            ScopeInterface::SCOPE_STORES,
            $this->storeManager->getStore()->getCode()
        );
    }
}
