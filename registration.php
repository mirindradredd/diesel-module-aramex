<?php
/**
 * @category  Beside
 * @package   Beside_Aramex
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Beside_Aramex', __DIR__);
