/**
 * Copyright © 2018 EaDesign by Eco Active S.R.L. All rights reserved.
 * See LICENSE for license details.
 */

var config = {

    config: {
        mixins: {
            'Magento_Checkout/js/action/set-shipping-information': {
                'Beside_Aramex/js/action/set-shipping-information-mixin': true
            }
        }
    }
};
