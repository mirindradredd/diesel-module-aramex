#Beside_Aramex Module

## Features

1) City & States
* Provides region/states for KSA and UAE
* City field has been converted to dropdown (Once FE improvement finishes)
* Structure is like Country>State>City. Upper level hierarchy is filtering lowest levels.
* Json structure is below
```
{
  "value": "816",
  "title": "Dubai",
  "country_id": "AE",
  "label": "Dubai",
  "cities": {
    "29": {
      "name": "Abu Hayl"
    },
    "30": {
      "name": "Al Qouz"
    },
    "31": {
      "name": "Bur Dubai"
    },
    "32": {
      "name": "Deira"
    },
    "33": {
      "name": "Dubai"
    },
    "34": {
      "name": "Shandaghah"
    },
    "35": {
      "name": "Dubai Internet City"
    },
    "36": {
      "name": "Jumeirah"
    },
    "37": {
      "name": "Karama"
    },
    "38": {
      "name": "Mina Sufooh"
    },
    "39": {
      "name": "Satwa"
    },
    "40": {
      "name": "Reem Community"
    },
    "41": {
      "name": "Jebal Ali"
    },
    "42": {
      "name": "Hatta"
    },
    "43": {
      "name": "Ganthoot"
    },
    "44": {
      "name": "Al Ruwaiya"
    }
  }
}
```
